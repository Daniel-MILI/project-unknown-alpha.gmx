move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

    if(place_meeting(x,y+1,Obj_Map_Terrain_01) || place_meeting(x,y+1,Obj_Map_Terrain_02)|| place_meeting(x,y+1,Obj_Map_Terrain_11) || place_meeting(x,y+1,Obj_Map_Terrain_12))
    {
        //Check if recently grounded
        if(!grounded && !key_jump){
            hkp_count = 0; //Init horizontal count
            jumping = false;
        }else if(grounded && key_jump){ //recently jumping
            jumping = true;
        }
    
        //Check if player grounded
        grounded = !key_jump;
        
        vsp = key_jump * -jumpspeed;
    }
    
    //Init hsp_jump_applied
    if(grounded){
        hsp_jump_applied = 0;
    }
    
    //Check horizontal counts
    if(move!=0 && grounded){
     hkp_count++;
    }else if(move==0 && grounded){
     hkp_count=0;
    }

//Check jumping
    if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}

//horizontal collision
    if(place_meeting(x+hsp,y,Obj_Map_Terrain_01))
    {
        while(!place_meeting(x+sign(hsp),y,Obj_Map_Terrain_01))
        {
            x+= sign(hsp);
        }
        hsp = 0;
    }
    else if(place_meeting(x+hsp,y,Obj_Map_Terrain_02))
    {
        while(!place_meeting(x+sign(hsp),y,Obj_Map_Terrain_02))
        {
            x+= sign(hsp);
        }
        hsp = 0;
    }
    else if(place_meeting(x+hsp,y,Obj_Map_Terrain_11))
    {
        while(!place_meeting(x+sign(hsp),y,Obj_Map_Terrain_11))
        {
            x+= sign(hsp);
        }
        hsp = 0;
    }
    else if(place_meeting(x+hsp,y,Obj_Map_Terrain_12))
    {
        while(!place_meeting(x+sign(hsp),y,Obj_Map_Terrain_12))
        {
            x+= sign(hsp);
        }
        hsp = 0;
    }
    else if(place_meeting(x+hsp,y,Obj_Enemy01_Static))
    {
        hsp_jump_applied = 0;
        while(!place_meeting(x+sign(hsp),y,Obj_Enemy01_Static))
        {
            x+= sign(hsp);
        }
        hsp = 0;
    }
    else if(place_meeting(x+hsp,y,Obj_Enemy02_Static))
    {
        
        while(!place_meeting(x+sign(hsp),y,Obj_Enemy02_Static))
        {
            x+= sign(hsp);

        }

        hsp=0;
    }
    else if(place_meeting(x+hsp,y,Obj_Movil_Ascensor))
    {
        
        while(!place_meeting(x+sign(hsp),y,Obj_Movil_Ascensor))
        {
            x+= sign(hsp);

        }
           
        hsp=0;
    }
    else if(place_meeting(x+hsp,y,Obj_Movil_Cinta))
    {
        
        while(!place_meeting(x+sign(hsp),y,Obj_Movil_Cinta))
        {
            x+= sign(hsp);

        }
            
        hsp=0;
    }


    x+= hsp;

//vertical collision
    if(place_meeting(x,y+vsp,Obj_Map_Terrain_01))
    {
        while(!place_meeting(x, sign(vsp) + y,Obj_Map_Terrain_01))
        {
            y+= sign(vsp);
        }
        vsp = 0;
    }
    else if(place_meeting(x,y+vsp,Obj_Map_Terrain_02))
    {
        while(!place_meeting(x, sign(vsp) + y,Obj_Map_Terrain_02))
        {
            y+= sign(vsp);
        }
        vsp = 0;
    }
    else if(place_meeting(x,y+vsp,Obj_Map_Terrain_11))
    {
        while(!place_meeting(x, sign(vsp) + y,Obj_Map_Terrain_11))
        {
            y+= sign(vsp);
        }
        vsp = 0;
    }
    else if(place_meeting(x,y+vsp,Obj_Map_Terrain_12))
    {
        while(!place_meeting(x, sign(vsp) + y,Obj_Map_Terrain_12))
        {
            y+= sign(vsp);
        }
        vsp = 0;
    }
    else if(place_meeting(x,y+vsp,Obj_Enemy01_Static))
    {
        hsp_jump_applied = 0;
        while(!place_meeting(x, sign(vsp) + y,Obj_Enemy01_Static))
        {
            y+= sign(vsp);
        }
        vsp = 0;
    }
    else if(place_meeting(x,y+vsp,Obj_Enemy02_Static))
    {
       hsp_jump_applied = 0;
        while(!place_meeting(x, sign(vsp) + y,Obj_Enemy02_Static))
        {
            y+= sign(vsp);
        
        }
       vsp = 0;
    }
     else if(place_meeting(x,y+vsp,Obj_Movil_Ascensor))
    {
       hsp_jump_applied = 0;
        while(!place_meeting(x, sign(vsp) + y,Obj_Movil_Ascensor))
        {
            y+= sign(vsp);
        
        }
         if(vsp > 0){
                y -= 10;
            }else{
                y += 10;
            }
       vsp = 0;
    }
     else if(place_meeting(x,y+vsp,Obj_Movil_Cinta))
    {
       hsp_jump_applied = 0;
        while(!place_meeting(x, sign(vsp) + y,Obj_Movil_Cinta))
        {
            y+= sign(vsp);
        
        }
       vsp = 0;
    }

    
    y+= vsp;


//ground colision IA
    //siera suelo
        if(fearofheight && place_meeting(x,y+8,Obj_Map_Terrain_11) && 
        !position_meeting (x+(sprite_width/2) *dir, y +(sprite_height/2)+8,Obj_Map_Terrain_11)){
         dir *= -1;
        }
        
        else if(fearofheight && place_meeting(x,y+8,Obj_Map_Terrain_12) && 
        !position_meeting (x+(sprite_width/2) *dir, y +(sprite_height/2)+8,Obj_Map_Terrain_12)){
         dir *= -1;
        }
        
    //siera techo
        if(fearofheight1 && place_meeting(x,y-8,Obj_Map_Terrain_11) && 
        !position_meeting (x+(sprite_width/2) *dir, y +(sprite_height/2)-8,Obj_Map_Terrain_11)){
         dir *= -1;
        }
        
        else if(fearofheight1 && place_meeting(x,y-8,Obj_Map_Terrain_12) && 
        !position_meeting (x+(sprite_width/2) *dir, y +(sprite_height/2)-8,Obj_Map_Terrain_12)){
         dir *= -1;
        }
    

