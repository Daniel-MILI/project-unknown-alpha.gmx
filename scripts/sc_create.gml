grav = 0.8;
hsp = 0;
vsp = 0;

movespeed = 4;

jumpspeed_normal = 8.2;
jumpspeed_powerup = 9.2;

jumpspeed = jumpspeed_normal;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//IA
fearofheight=0;
fearofheight1=0;
fearofheight2=0;
dir=-1;

//Init variables
key_left = 0;
key_right = 0;
key_up = 0;
key_jump = false;

//puntos
Puntos = 0;

exists= true;
